use gfx;
use glutin;

use gfx::traits::FactoryExt;
use gfx::{Device, Factory, Resources};
use gfx_window_glutin;

use lyon::extra::rust_logo::build_logo_path;
use lyon::path_builder::*;
use lyon::path_iterator::*;
use lyon::math::*;
use lyon::tessellation::geometry_builder::{VertexConstructor, VertexBuffers, BuffersBuilder};
use lyon::tessellation::{FillTessellator, FillOptions};
use lyon::tessellation;
use lyon::path::Path;

use ::shapes::*;
use std::sync::mpsc::{Sender, Receiver, channel};

type PostmodernRect = ::shapes::Rect;
pub(crate) type ColorFormat = gfx::format::Srgba8;
pub(crate) type DepthFormat = gfx::format::DepthStencil;

gfx_defines!{
    vertex GpuFillVertex {
        position: [f32; 2] = "a_position",
    }

    pipeline fill_pipeline {
        vbo: gfx::VertexBuffer<GpuFillVertex> = (),
        out_color: gfx::RenderTarget<ColorFormat> = "out_color",
    }
}

// A very simple vertex constructor that only outputs the vertex position
struct VertexCtor;
impl VertexConstructor<tessellation::FillVertex, GpuFillVertex> for VertexCtor {
    fn new_vertex(&mut self, vertex: tessellation::FillVertex) -> GpuFillVertex {
        assert!(!vertex.position.x.is_nan());
        assert!(!vertex.position.y.is_nan());
        GpuFillVertex {
            // (ugly hack) tweak the vertext position so that the logo fits roughly
            // within the (-1.0, 1.0) range.
            position: (vertex.position * 0.0145 - vec2(1.0, 1.0)).to_array(),
        }
    }
}

fn upoint(x: u64, y: u64) -> Point {
    point(x as f32, y as f32)
}

fn draw_rect(rect: PostmodernRect, tess: &mut FillTessellator, mesh: &mut VertexBuffers<GpuFillVertex>) {
    let mut builder = Path::builder();

    builder.move_to(upoint(rect.x, rect.y));
    builder.line_to(upoint(rect.x, rect.y + rect.height));
    builder.line_to(upoint(rect.x + rect.width, rect.y + rect.height));
    builder.line_to(upoint(rect.x + rect.width, rect.y));
    builder.line_to(upoint(rect.x, rect.y));
    builder.close();

    let path = builder.build();

    tess.tessellate_path(
        path.path_iter(),
        &FillOptions::default().with_tolerance(0.01),
        &mut BuffersBuilder::new(mesh, VertexCtor),
    ).unwrap();
}

pub(crate) enum PostmodernEvent {
    RequestFrame(PostmodernRect),
    Click {x: u64, y: u64},
    Close,
}

pub(crate) fn main(frames: Receiver<Vec<PostmodernRect>>, events_out: Sender<PostmodernEvent>) {
    let mut tessellator = FillTessellator::new();

    // Initialize glutin and gfx-rs (refer to gfx-rs examples for more details).
    let events_loop = glutin::EventsLoop::new();
    let glutin_builder = glutin::WindowBuilder::new()
        .with_dimensions(700, 700)
        .with_decorations(true)
        .with_title("Postmodern UI".to_string())
        .with_vsync();

    let (window, mut device, mut factory, mut main_fbo, mut main_depth) =
        gfx_window_glutin::init::<ColorFormat, DepthFormat>(glutin_builder, &events_loop);

    let shader = factory.link_program(
        VERTEX_SHADER.as_bytes(),
        FRAGMENT_SHADER.as_bytes()
    ).unwrap();

    let pso = factory.create_pipeline_from_program(
        &shader,
        gfx::Primitive::TriangleList,
        gfx::state::Rasterizer::new_fill(),
        fill_pipeline::new(),
    ).unwrap();

    let mut cmd_queue: gfx::Encoder<_, _> = factory.create_command_buffer().into();

    loop {
        if !update_inputs(&events_loop, events_out.clone()) {
            break;
        }

        gfx_window_glutin::update_views(&window, &mut main_fbo, &mut main_depth);

        {
            let mut mesh = VertexBuffers::new();
            let bounding_box = PostmodernRect {x: 0, y: 0, width: 700, height: 700};
            events_out.send(PostmodernEvent::RequestFrame(bounding_box))
                .expect("Failed request frame");
            if let Ok(mut frames) = frames.try_recv() {
                println!(" -- {:?}", frames);
                for rect in frames.drain(..) {
                    draw_rect(rect, &mut tessellator, &mut mesh);
                }
                println!(" -- fill: {} vertices {} indices", mesh.vertices.len(), mesh.indices.len());

                let (vbo, ibo) = factory.create_vertex_buffer_with_slice(
                    &mesh.vertices[..],
                    &mesh.indices[..]
                );

                cmd_queue.clear(&main_fbo.clone(), [0.8, 0.8, 0.8, 1.0]);
                cmd_queue.draw(
                    &ibo,
                    &pso,
                    &fill_pipeline::Data {
                        vbo: vbo.clone(),
                        out_color: main_fbo.clone(),
                    },
                );
                cmd_queue.flush(&mut device);
            }
        }

        window.swap_buffers().unwrap();

        device.cleanup();
    }
    events_out.send(PostmodernEvent::Close).expect("Other side is already closed");
}

fn update_inputs(events_loop: &glutin::EventsLoop, events_out: Sender<PostmodernEvent>) -> bool {
    let mut running = true;
    events_loop.poll_events(|glutin::Event::WindowEvent{window_id: _, event}| {
        use glutin::WindowEvent::*;
        use glutin::VirtualKeyCode;
        match event {
            Closed |
            KeyboardInput(_, _, Some(VirtualKeyCode::Escape), _) => {
                running = false;
            }
            _evt => {
                //println!("{:?}", _evt);
            }
        };
    });

    running
}

pub static VERTEX_SHADER: &'static str = &"
    #version 140
    #line 266

    in vec2 a_position;

    out vec4 v_color;

    void main() {
        gl_Position = vec4(a_position, 0.0, 1.0);
        gl_Position.y *= -1.0;
        v_color = vec4(0.0, 0.0, 0.0, 1.0);
    }
";

// The fragment shader is dead simple. It just applies the color computed in the vertex shader.
// A more advanced renderer would probably compute texture coordinates in the vertex shader and
// sample the color from a texture here.
pub static FRAGMENT_SHADER: &'static str = &"
    #version 140
    in vec4 v_color;
    out vec4 out_color;

    void main() {
        out_color = v_color;
    }
";
