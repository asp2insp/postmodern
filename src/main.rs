#![allow(unused)]

extern crate glutin;
#[macro_use]
extern crate gfx;
extern crate gfx_window_glutin;
extern crate lyon;

use std::io;
use std::sync::mpsc::{Sender, Receiver, channel};
use std::sync::Arc;
use std::thread;

mod render;
mod shapes;
use shapes::*;
use render::PostmodernEvent;

macro_rules! reducer {
    (type $reducer:ident = ($msg:ty)->struct $store:ident { $($n:ident: $t:ty),+ $(,)* }) => {
        #[derive(Debug, Clone)]
        struct $store {
            $($n: $t,)*
        }
        #[allow(non_camel_case_types)]
        struct $reducer<$($n: Fn($t, $msg)->$t,)*> {
            $($n: $n,)*
            initial_state: $store,
        }
        #[allow(non_camel_case_types)]
        impl <$($n,)*> $reducer<$($n,)*>
        where $($n : Fn($t, $msg)->$t,)* {
            fn recv(self, m: $msg) -> Self {
                let mut this = self;
                $(this.initial_state.$n = (this.$n)(this.initial_state.$n, m);)*
                this
            }

            fn get_state(&self) -> $store {
                self.initial_state.clone()
            }
        }
    };
}

macro_rules! selector {
    (type $selector:ident = ($store:ty)->struct $selout:ident { $($n:ident: $t:ty),+ $(,)* }) => {
        #[derive(Debug)]
        struct $selout {
            $($n: $t,)*
        }
        #[allow(non_camel_case_types)]
        struct $selector<$($n: Fn(&$store)->$t,)*> {
            $($n: $n,)*
        }
        #[allow(non_camel_case_types)]
        impl <$($n,)*> $selector<$($n,)*>
        where $(for <'r> $n : Fn(&'r $store)->$t,)* {
            fn compute(&self, s: &$store) -> $selout {
                $selout {
                    $($n: (self.$n)(s),)*
                }
            }
        }
    };
}


type WidgetId = u64;

struct Widget {
    children: Vec<WidgetId>,
    name: String,
    click_handler: Option<Box<Fn()->()>>,
    position: Box<Positioning>,
    opaque: bool,
}

impl Widget {
    fn click(&self) {
        self.click_handler.as_ref().map(|handler| handler());
    }
}

#[must_use]
struct WidgetBuilder {
    name: Option<String>,
    click_handler: Option<Box<Fn()->()>>,
    position: Option<Box<Positioning>>,
    child_builders: Vec<WidgetBuilder>,
    opaque: bool,
}

impl <Msg, C: Component<Msg>> From<(Actions<Msg>, C)> for WidgetBuilder {
    fn from((a, c): (Actions<Msg>, C)) -> WidgetBuilder {
        c.render(a)
    }
}

// Should I allow both directions? ie From<(C, Actions)> as well?

impl WidgetBuilder {

    fn new() -> WidgetBuilder {
        WidgetBuilder {
            name: None,
            click_handler: None,
            position: None,
            child_builders: Vec::new(),
            opaque: false,
        }
    }

    fn name<N: Into<String>>(self, name: N) -> WidgetBuilder {
        let mut builder = self;
        builder.name = Some(name.into());
        builder
    }

    fn position<P: Positioning + 'static>(self, pos: P) -> WidgetBuilder {
        let mut builder = self;
        builder.position = Some(Box::new(pos));
        builder
    }

    fn opaque(self, opaque: bool) -> WidgetBuilder {
        let mut builder = self;
        builder.opaque = opaque;
        builder
    }

    fn on_click<F: Fn()->() + 'static>(self, handler: F) -> WidgetBuilder {
        let mut builder = self;
        builder.click_handler = Some(Box::new(handler));
        builder
    }

    fn on_click_mut<F: Fn()->() + 'static>(&mut self, handler: F) {
        self.click_handler = Some(Box::new(handler));
    }

    fn child<C: Into<WidgetBuilder>>(self, child: C) -> WidgetBuilder {
        let mut builder = self;
        builder.child_builders.push(child.into());
        builder
    }

    fn child_mut<C: Into<WidgetBuilder>>(&mut self, child: C) {
        self.child_builders.push(child.into());
    }

    fn build(self, context: &mut Context) -> WidgetId {
        let mut builder = self;
        let children = builder.child_builders
                        .drain(..)
                        .map(|child| child.build(context))
                        .collect();
        let position = builder.position.unwrap_or(Box::new(BoxModel::default()));
        context.add_widget(Widget {
            name: builder.name.expect("Widget name was not set on the builder"),
            click_handler: builder.click_handler,
            position: position,
            children: children,
            opaque: builder.opaque,
        })
    }
}


struct Context {
    arena: Vec<Widget>,
    // resolved_sizes: Vec<Option<Rect>>, // TODO: would this speed things up?
}

impl Context {
    fn new() -> Context {
        Context {
            arena: Vec::new(),
        }
    }

    fn resolve_sizes(&mut self, root: WidgetId, bounding_box: Rect) {
        let node_pos = {
            let root_node = &mut self.arena[root as usize];
            let node_pos = root_node.position.final_position(bounding_box);
            root_node.position = Box::new(node_pos);
            node_pos
        };
        let mut children = self.arena[root as usize].children.clone();
        for cw in children.drain(..) {
            self.resolve_sizes(cw, node_pos);
        }
    }

    fn to_compositor(&mut self, root: WidgetId, bounding_box: Rect) -> Vec<Rect> {
        self.arena.iter()
            .filter(|w| w.opaque)
            .map(|c| c.position.final_position(DEFAULT_POS))
            .collect()
    }

    fn reset(&mut self) {
        self.arena.clear();
    }

    fn add_widget(&mut self, w: Widget) -> WidgetId {
        let id = self.arena.len() as u64;
        self.arena.push(w);
        id
    }

    fn find_widget_by_text<'s, S: Into<&'s str>>(&self, text: S) -> Option<WidgetId> {
        let cmp = text.into();
        for (i, w) in self.arena.iter().enumerate() {
            if w.name == cmp {
                return Some(i as WidgetId)
            }
        }
        None
    }

    fn click(&self, root: WidgetId, target: WidgetId) -> bool {
        if root == target {
            match self.arena[root as usize].click_handler {
                Some(ref handler) => {
                    handler();
                    false
                },
                None => true, // Click needs to be performed
            }
        } else {
            let w = &self.arena[root as usize];
            if w.children.iter().any(|child| self.click(*child, target)) {
                match w.click_handler {
                    Some(ref handler) => {
                        handler();
                        false
                    },
                    None => true,
                }
            } else {
                false
            }
        }

    }
}

fn fmt_widget(id: WidgetId, c: &Context, depth: usize, f: &mut io::Write) -> io::Result<()> {
    let w = &c.arena[id as usize];
    let pos = w.position.final_position(DEFAULT_POS);
    if w.children.len() == 0 {
        write!(f, "{:indent$}[{name}{pos}]\n", "", indent=depth, name=w.name, pos=pos)
    } else {
        write!(f, "{:indent$}{name}{pos} {{\n", "", indent=depth, name=w.name, pos=pos)?;
        for cw in w.children.iter().cloned() {
            fmt_widget(cw, c, depth+2, f)?;
        }
        write!(f, "{:indent$}}}\n", "", indent=depth)
    }
}

fn w<N: Into<String>>(name: N) -> WidgetBuilder {
    WidgetBuilder::new().name(name)
}

type Actions<Msg> = Arc<Fn(Msg)->()>;

fn wrapping<InType, OutType, Conv>(destination: Actions<OutType>, conv: Conv) -> Actions<InType>
where InType: 'static, OutType: 'static, Conv: Fn(InType)->OutType + 'static{
    Arc::new(move |msg: InType| destination(conv(msg)))
}

fn convert_channel<Msg: 'static>(tx: Sender<Msg>) -> Actions<Msg> {
    Arc::new(move |m| tx.send(m).unwrap())
}

trait Component<Msg> {
    fn render(self, Actions<Msg>) -> WidgetBuilder;
}


fn main() {
    enum TodoMsg {
        Toggle(usize),
    }

    reducer! {
        type Reducer = (TodoMsg) -> struct TodoStore {
            todos: Vec<(bool, String)>,
        }
    };
    let reducers = Reducer {
        todos: |state, action| {
            match action {
                TodoMsg::Toggle(index) => {
                    let mut state = state;
                    state[index].0 = !state[index].0;
                    state
                },
            }
        },
        initial_state: TodoStore {
            todos: vec![
                (true, "Solve multitype traversal problem".into()),
                (false, "Incorporate Actions prototype".into()),
            ],
        },
    };

    selector! {
        type Selector = (TodoStore) -> struct TodoSelectors {
            remaining: usize,
            completed: usize,
        }
    };
    let selectors = Selector {
        remaining: |store| store.todos.iter().filter(|i| !i.0).count(),
        completed: |store| store.todos.iter().filter(|i| i.0).count(),
    };

    // Now let's bind some UI
    struct Checkbox {
        key: usize,
        checked: bool,
        label: String,
    }

    impl Component<TodoMsg> for Checkbox {
        fn render(self, actions: Actions<TodoMsg>) -> WidgetBuilder {
            let ic_char = if self.checked { '✔' } else { ' ' };
            let index = self.key;
            w("row")
                .child(
                    w(format!("Icon({})", ic_char))
                        .position(BoxModel {
                            x: Some(Value::Percent(5)),
                            y: Some(Value::Percent(5)),
                            width: Some(Value::Percent(10)),
                            height: Some(Value::Percent(90)),
                        })
                        .opaque(true)
                )
                .child(
                    w(self.label)
                        .position(BoxModel {
                            x: Some(Value::Percent(20)),
                            y: Some(Value::Percent(5)),
                            width: Some(Value::Percent(80)),
                            height: Some(Value::Percent(90)),
                        })
                        .opaque(true)
                )
                .position(BoxModel {
                    y: Some(Value::Points(index as u64 * 30)),
                    height: Some(Value::Points(30)),
                    ..Default::default()
                })
                .on_click(move || actions(TodoMsg::Toggle(index)) )
        }
    }

    struct TodoList {
        todos: Vec<(bool, String)>,
    }

    impl Component<TodoMsg> for TodoList {
        fn render(mut self, actions: Actions<TodoMsg>) -> WidgetBuilder {
            let mut builder = w("list");
            let height = self.todos.len() as u64 * 30;
            for todo in self.todos.drain(..).enumerate() {
                let index = todo.0;
                let item = todo.1;
                let y = index as u64 * 30u64;
                builder.child_mut(
                    (
                        actions.clone(),
                        Checkbox { key: index, checked: item.0, label: item.1 }
                    )
                );
            }
            builder.position(BoxModel {
                height: Some(Value::Points(height)),
                ..Default::default()
            })
        }
    }

    fn root(store: TodoStore, actions: Actions<TodoMsg>) -> WidgetBuilder {
        w("panel")
            .child((actions, TodoList {
                todos: store.todos,
            }))
    }

    // This code goes into the engine mostly
    { // Pass/Capture: Reducers, Selectors, Root Component


        // Process messages
        // let target = c.find_widget_by_text("Incorporate Actions prototype")
        //               .expect("Couldn't find by text");
        // c.click(r, target);
        // reducers = reducers.recv(rx.recv().unwrap());

        let (frames_tx, frames_rx) = channel();
        let (events_tx, events_rx) = channel();

        thread::spawn(move || {
            let mut reducers = reducers;
            let mut selectors = selectors;

            let mut c = Context::new();
            let (tx, rx) = channel::<TodoMsg>();

            let mut event_buf: Vec<PostmodernEvent> = Vec::new();
            loop {
                let event_res = events_rx.recv();
                match event_res {
                    Ok(event) => event_buf.push(event),
                    Err(_) => break,
                };
                while let Ok(event) = events_rx.try_recv() {
                    event_buf.push(event);
                }

                let mut request_frame: Option<Rect> = None;
                for event in event_buf.drain(..) {
                    match event {
                        PostmodernEvent::Close => break,
                        PostmodernEvent::Click {x, y} => {
                            // TODO
                        },
                        PostmodernEvent::RequestFrame(bounding_box) => request_frame = Some(bounding_box),
                    };
                }

                request_frame.map(|bounding_box| {
                    c.reset();
                    let r = root(reducers.get_state(), convert_channel(tx.clone())).build(&mut c);
                    c.resolve_sizes(r, bounding_box);
                    fmt_widget(r, &c, 0, &mut io::stdout()).unwrap();
                    let frames = c.to_compositor(r, bounding_box);
                    frames_tx.send(frames).expect("compositor hung up");
                });
            }
        });

        render::main(frames_rx, events_tx);
    }

}
