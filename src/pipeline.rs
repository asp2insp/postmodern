trait Style {
    fn get_property<T>(&self, Vec<String> prop_path) -> Option<T>; // TODO maybe support serde here?
}

impl Style {
    fn overlay(&self, top: Style) -> OverlayStyle {
        OverlayStyle {
            base: self,
            top: top,
        }
    }
}

struct OverlayStyle {
    base: Style,
    top: Style,
}

impl Style for OverlayStyle {
    fn get_property<T>(&self, Vec<String> prop_path) -> Option<T> {
        self.top.get_property(prop_path).or_else(|| self.base.get_property(prop_path));
    }
}

trait ActionHandler {
    fn on_click(&self);
    fn on_touch_start(&self);
    fn on_touch_move(&self);
    fn on_touch_end(&self);
    fn on_drag(&self);
    // etc
}

type Tick = (usize, usize);

trait Animator {
    fn animate(Drawable, AbsolutePositioning, Tick);
}

struct Node<Style, Template, Positioning, AH: ActionHandler, AN: Animator> {
    style: Style,
    template: Template,
    positioning: Positioning,
    handler: AH,
    animator: AN,
}

struct AbsolutePositioning {
    x: usize,
    y: usize,
    width: usize,
    height: usize,
    r: usize,
}

struct Drawable {
    contents: Vec<Svg>, // TODO what's the format here? SVG probably?
}
