#![allow(dead_code)]
use std::f32;

pub struct FlexStyle {
    display: Display,
    flex_direction: FlexDirection,
    flex_wrap: FlexWrap,
    order: u32,
    flex_grow: u32,
    flex_shrink: u32,
    flex_basis: Option<u32>,
    justify_content: JustifyContent,
    align_items: AlignItems,
    align_self: AlignSelf,
    align_content: AlignContent,

    // Layout props which affect flex
    margin: EdgeValues,
    padding: EdgeValues,
    border: EdgeValues,

    // More inherent properties
    width: FlexUnit,
    height: FlexUnit,
    aspect_ratio: f32,
}

pub struct FlexNode {
    style: FlexStyle,
    dirty: bool,

}


macro_rules! default_enum{
    ($e:ty, $d:expr) => (
        impl Default for $e {
            fn default() -> $e {
                $d
            }
        }
    );
}

impl Default for FlexStyle {
    fn default() -> FlexStyle {
        FlexStyle {
            display: Default::default(),
            flex_direction: Default::default(),
            flex_wrap: Default::default(),
            order: Default::default(),
            flex_grow: Default::default(),
            flex_shrink: 1,
            flex_basis: None,
            justify_content: Default::default(),
            align_items: Default::default(),
            align_self: Default::default(),
            align_content: Default::default(),

            margin: Default::default(),
            padding: Default::default(),
            border: Default::default(),

            width: Default::default(),
            height: Default::default(),
            aspect_ratio: f32::NAN,
        }
    }
}

pub enum FlexBasis {
    Auto, // Look at main axis size
    Fill,
    Content,
    Size(f32),
}
default_enum!(FlexBasis, FlexBasis::Auto);

pub enum EdgeValues {
    None,
    All(f32),
    VerticalHorizontal(f32, f32),
    TopHorizontalBottom(f32, f32, f32),
    TopEndBottomStart(f32, f32, f32, f32),
}
default_enum!(EdgeValues, EdgeValues::None);

pub enum FlexUnit { Points(f32), Percent(f32), Undefined }
default_enum!(FlexUnit, FlexUnit::Undefined);
pub enum TextDirection { LTR, RTL }
default_enum!(TextDirection, TextDirection::LTR);
pub enum Display { Flex, InlineFlex }
default_enum!(Display, Display::Flex);
pub enum FlexDirection { Row, RowReverse, Column, ColumnReverse }
default_enum!(FlexDirection, FlexDirection::Row);
pub enum FlexWrap { NoWrap, Wrap, WrapReverse }
default_enum!(FlexWrap, FlexWrap::NoWrap);
pub enum JustifyContent { FlexStart, FlexEnd, Center, SpaceBetween, SpaceAround }
default_enum!(JustifyContent, JustifyContent::FlexStart);
pub enum AlignItems { FlexStart, FlexEnd, Center, Baseline, Stretch }
default_enum!(AlignItems, AlignItems::FlexStart);
pub enum AlignSelf { Auto, FlexStart, FlexEnd, Center, Baseline, Stretch }
default_enum!(AlignSelf, AlignSelf::Auto);
pub enum AlignContent { Stretch, FlexStart, FlexEnd, Center, SpaceBetween, SpaceAround }
default_enum!(AlignContent, AlignContent::Stretch);
