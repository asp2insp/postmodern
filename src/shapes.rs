use std::fmt;

#[derive(Copy, Clone)]
pub(crate) enum Value {
    Points(u64),
    Percent(u64),
}

impl Default for Value {
    fn default() -> Value {
        Value::Points(0)
    }
}

#[derive(Copy, Clone, Debug)]
pub(crate) struct Rect {
    pub(crate) x: u64,
    pub(crate) y: u64,
    pub(crate) width: u64,
    pub(crate) height: u64,
}

impl fmt::Display for Rect {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {}, {}, {})", self.x, self.y, self.width, self.height)
    }
}

pub(crate) const DEFAULT_POS: Rect = Rect { x: 0, y: 0, width: 0, height: 0 };

pub(crate) trait Positioning {
    fn final_position(&self, parent: Rect) -> Rect;
}

impl Positioning for Rect {
    fn final_position(&self, _: Rect) -> Rect {
        *self
    }
}

#[derive(Default)]
pub(crate) struct BoxModel {
    pub(crate) x: Option<Value>,
    pub(crate) y: Option<Value>,
    pub(crate) width: Option<Value>,
    pub(crate) height: Option<Value>,
}

pub(crate) fn calc_value(val: Value, parent: u64) -> u64 {
    use Value::*;
    match val {
        Points(n) => n,
        Percent(n) => parent * n / 100,
    }
}

impl Positioning for BoxModel {
    fn final_position(&self, parent: Rect) -> Rect {
        Rect {
            x: parent.x + self.x
                       .map(|val| calc_value(val, parent.width))
                       .unwrap_or(0),
            y: parent.y + self.y
                       .map(|val| calc_value(val, parent.height))
                       .unwrap_or(0),
            width: self.width
                       .map(|val| calc_value(val, parent.width))
                       .unwrap_or(parent.width),
            height: self.height
                        .map(|val| calc_value(val, parent.height))
                        .unwrap_or(parent.height),
        }
    }
}
