# UI as a Pipeline

Thinking about the user interface framework through a functional lens creates a pipeline mentality. Here's how postmodern constructs this pipe:

```
[Data]    Your application's model
  |
  | <----- View functions take data and produce widget Nodes
  v
[Abstract UI Tree]  Consists of: tree of Nodes.
  |                 each Node has a Template, Style data, Action handlers,
  |                 Positioning data, and Animations
  |
  | <------ Layout Engine takes Positioning data and modifies each node
  |         to have Absolute Positioning data
  |
  | <------ Template engine applies Style data to the Template and
  |         and produces a Drawable Payload
  |
  | <------ Animation engine modifies the Drawable or the Positioning
  |         data to set them to new values
  |
  v
[UI objects]  At this point the UI done and just needs to be rendered.
  |           it consists of a Drawable, a Position, and the Action
  |           handlers.
  |
  | <------ Rendering engine draws to the screen
  v
[Pixels]
  |         User interaction completes the cycle, triggering a re-run
  |         of the pipeline.
  |
  | <------ User input triggers action handlers
  |
  v
[Actions/Messages]   a-la Elm/Redux
  |
  | <------ Reducers take the message and the existing state and produce
  |         a new transformed state
  v
[Data]  Go to top of pipeline.
```
